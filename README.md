### What is this repository for? ###

Basic EKF SLAM simulation with Python and SciPy

### How do I get set up? ###

* create virtualenv
* install numpy and matplotlib via ```pip install <package>```
* run tests ```make all``` (runs simulation)

### License ###
MIT